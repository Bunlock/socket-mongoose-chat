var app = require('express')();
var http = require('http').Server(app);
var socket = require('socket.io')(http);
var moment = require('moment');
moment.locale('fr');


var port = process.env.PORT || 3000;
const mongoose = require('mongoose');
let people = {};
mongoose.connect('mongodb://localhost/chatlog',{ useNewUrlParser: true });
mongoose.set('debug', true);
mongoose.connection
    .on('error', console.error.bind(console, 'connection error:'))
    .once('open', function() {
        console.log("mongoose connected! ")
    });
	
app.get('/', function(req, res){
  res.sendFile(__dirname + '/chat.html');
});
const chatLogSchema = new mongoose.Schema({
	time: String,
	user: String,
	msg: String
})

const chatLogModel = mongoose.model('chatlog', chatLogSchema);

socket.on("connection", function (client) {

	client.on("join", function(name){
		people[client.id] = name;
		client.emit("update", "You have connected to the server.");
		socket.sockets.emit("update", name + " has joined the server.")
		socket.sockets.emit("update-people", people);
	});

	client.on("send", function(msg){
		
		let date = moment().format('LLLL');
		console.log(date);
		let chatLogItem = new chatLogModel({time: date, user:people[client.id], msg: msg});
	
		chatLogItem.save().then(()=>{console.log("record added")});;
		socket.sockets.emit("chat", people[client.id], msg);
	});

	client.on("disconnect", function(){
		socket.sockets.emit("update", people[client.id] + " has left the server.");
		delete people[client.id];
		socket.sockets.emit("update-people", people);
	});
	
	client.on("list", function(){
		chatLogModel.find({}).then((doc)=>{
			client.emit("list",doc)});
	});
});
http.listen(port, function(){
  console.log('listening on *:' + port); 
});